package org.rhedcloud.rtp.app;

import java.util.List;
import java.util.Properties;

//Core Java
import javax.jms.Message;
import javax.jms.TextMessage;

import org.apache.logging.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.openeai.config.AppConfig;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.config.PropertyConfig;
import org.openeai.jms.consumer.commands.CommandException;
import org.openeai.jms.consumer.commands.RequestCommand;
import org.openeai.jms.consumer.commands.RequestCommandImpl;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.moa.ActionableEnterpriseObject;
import org.openeai.moa.EnterpriseObjectCreateException;
import org.openeai.moa.EnterpriseObjectDeleteException;
import org.openeai.moa.EnterpriseObjectQueryException;
import org.openeai.moa.XmlEnterpriseObjectException;
import org.openeai.xml.XmlDocumentReader;
import org.openeai.xml.XmlDocumentReaderException;
import org.rhedcloud.moa.jmsobjects.tagging.v1_0.ResourceTaggingProfile;
import org.rhedcloud.moa.objects.resources.v1_0.ManagedTag;
import org.rhedcloud.moa.objects.resources.v1_0.ResourceTaggingProfileQuerySpecification;

public class Create {

	public static void main(String[] args) throws EnterpriseConfigurationObjectException, XmlEnterpriseObjectException, EnterpriseFieldException, EnterpriseObjectDeleteException, EnterpriseObjectCreateException {
		System.out.println("Loading AppConfig");
		AppConfig appConfig = new AppConfig(args[0],args[1]);
		System.out.println("Done loading AppConfig");
		PointToPointProducer p2p =
				(PointToPointProducer) appConfig.getObject("QueryAppP2PProducer");
		ResourceTaggingProfile aeo = (ResourceTaggingProfile) appConfig.getObject("ResourceTaggingProfile.v1_0");
		aeo.setProfileName("profile-b");
		aeo.setNamespace("test");
		aeo.setRevision("1");
		aeo.setProfileId("666"); // Note, this gets ignored!
//		ManagedTag mt = aeo.newManagedTag();
//		mt.setTagName("tagname-x");
//		mt.setTagValue("tagvalue-x");
//		aeo.addManagedTag(mt);
		System.out.println(aeo.create(p2p));
		System.exit(0);

	}

}
