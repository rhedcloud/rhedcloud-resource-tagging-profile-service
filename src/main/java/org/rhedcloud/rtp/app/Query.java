package org.rhedcloud.rtp.app;

import java.util.List;

//Core Java
import javax.jms.Message;
import javax.jms.TextMessage;

import org.apache.logging.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.moa.EnterpriseObjectQueryException;
import org.openeai.moa.XmlEnterpriseObjectException;
import org.rhedcloud.moa.jmsobjects.tagging.v1_0.ResourceTaggingProfile;
import org.rhedcloud.moa.objects.resources.v1_0.ResourceTaggingProfileQuerySpecification;

public class Query {

	public static void main(String[] args) throws EnterpriseConfigurationObjectException, EnterpriseObjectQueryException, XmlEnterpriseObjectException, EnterpriseFieldException {
		System.out.println("Loading AppConfig");
		AppConfig appConfig = new AppConfig(args[0],args[1]);
		System.out.println("Done loading AppConfig");
		PointToPointProducer p2p =
				(PointToPointProducer) appConfig.getObject("QueryAppP2PProducer");
		ResourceTaggingProfileQuerySpecification querySpec =
				(ResourceTaggingProfileQuerySpecification) appConfig.getObject("ResourceTaggingProfileQuerySpecification.v1_0");
		querySpec.setProfileId("42");
//		querySpec.setNamespace("cimp");
//		querySpec.setProfileName("clinicaltrials");
		ResourceTaggingProfile aeo = (ResourceTaggingProfile) appConfig.getObject("ResourceTaggingProfile.v1_0");
		List<ResourceTaggingProfile> profiles = aeo.query(querySpec, p2p);
		System.out.println(profiles.size()+" profile(s) returned.");
		System.out.println("<profiles>");
		for (ResourceTaggingProfile profile: profiles) {
			System.out.println(profile.toXmlString());
		}
		System.out.println("</profiles>");
		System.exit(0);
	}

}
