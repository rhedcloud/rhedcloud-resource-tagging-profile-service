package org.rhedcloud.rtp.app;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;

//Core Java
import javax.jms.Message;
import javax.jms.TextMessage;

import org.apache.logging.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.openeai.config.AppConfig;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.config.PropertyConfig;
import org.openeai.jms.consumer.commands.CommandException;
import org.openeai.jms.consumer.commands.RequestCommand;
import org.openeai.jms.consumer.commands.RequestCommandImpl;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.moa.ActionableEnterpriseObject;
import org.openeai.moa.EnterpriseObjectCreateException;
import org.openeai.moa.EnterpriseObjectDeleteException;
import org.openeai.moa.EnterpriseObjectQueryException;
import org.openeai.moa.XmlEnterpriseObjectException;
import org.openeai.xml.XmlDocumentReader;
import org.openeai.xml.XmlDocumentReaderException;
import org.rhedcloud.moa.jmsobjects.tagging.v1_0.ResourceTaggingProfile;
import org.rhedcloud.moa.objects.resources.v1_0.ManagedTag;
import org.rhedcloud.moa.objects.resources.v1_0.ResourceTaggingProfileQuerySpecification;

public class CreateAll {

	public static void main(String[] args) throws EnterpriseConfigurationObjectException, XmlEnterpriseObjectException, EnterpriseFieldException, EnterpriseObjectDeleteException, EnterpriseObjectCreateException, EnterpriseObjectQueryException {
		try {
			AppConfig appConfig = new AppConfig(args[0],args[1]);
			PointToPointProducer p2p = 
					(PointToPointProducer) appConfig.getObject("QueryAppP2PProducer");
			
			ResourceTaggingProfileQuerySpecification querySpec = 
					(ResourceTaggingProfileQuerySpecification) appConfig.getObject("ResourceTaggingProfileQuerySpecification.v1_0");

			// open a file and strip out the ResourceTaggingProfile
			String rtpsString = readFileAsString("deploy/build-test/message/releases/org/rhedcloud/Tagging/ResourceTaggingProfile/1.0/xml/ResourceTaggingProfile-SampleData.xml");
			//		System.out.println(rtpString);
			int endIndex = rtpsString.indexOf("</DataArea>");
			int beginIndex = rtpsString.indexOf("<DataArea>")+"<DataArea>".length();
			String dataareaString = rtpsString.substring(beginIndex, endIndex);
			//		System.out.println(dataareaString);
			while (dataareaString.contains("<ResourceTaggingProfile")) {
				endIndex = dataareaString.indexOf("</ResourceTaggingProfile>")+"</ResourceTaggingProfile>".length();
				beginIndex = dataareaString.indexOf("<ResourceTaggingProfile");
				String rtpString = dataareaString.substring(beginIndex, endIndex);
				dataareaString = dataareaString.substring(endIndex);
				ResourceTaggingProfile aeo = (ResourceTaggingProfile) appConfig.getObject("ResourceTaggingProfile.v1_0");
				aeo.buildObjectFromXmlString(rtpString);
				System.out.println("Creating RTP: "+aeo.toXmlString());
				System.out.println(aeo.create(p2p));
				querySpec.setNamespace(aeo.getNamespace());
				querySpec.setProfileName(aeo.getProfileName());
				querySpec.setRevision(aeo.getRevision());
				ResourceTaggingProfile qeo = (ResourceTaggingProfile) appConfig.getObject("ResourceTaggingProfile.v1_0");
				List<ResourceTaggingProfile> profiles = qeo.query(querySpec, p2p);
				if (profiles.size() == 1) {
					ResourceTaggingProfile profile = profiles.get(0);
					profile.setProfileId(null);
					for(ManagedTag tag: (List<ManagedTag>)profile.getManagedTag()) {
						tag.setManagedTagId(null);
					}
					if (profile.equals(aeo)) {
						System.out.println("Verified.");
					} else {
						System.err.println("Not verified. Created profile not equal to the one from the query result.");
						System.out.println("Profile returned from query: "+profile.toXmlString());
					}
				} else {
					System.err.println("Not verified. Expected 1 query result, got "+profiles.size());
				}
				//			System.out.println(aeo.toXmlString());
				System.out.println("----------");
//				break;
			}
			System.exit(0);
		} catch(Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	public static String readFileAsString(String fileName) {
		String text = "";
		try {
			text = new String(Files.readAllBytes(Paths.get(fileName)));
		} catch (IOException e) {
			e.printStackTrace();
		}

		return text;
	}

}
