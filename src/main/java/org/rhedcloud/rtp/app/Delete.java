package org.rhedcloud.rtp.app;

import java.util.List;
import java.util.Properties;

//Core Java
import javax.jms.Message;
import javax.jms.TextMessage;

import org.apache.logging.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.openeai.config.AppConfig;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.config.PropertyConfig;
import org.openeai.jms.consumer.commands.CommandException;
import org.openeai.jms.consumer.commands.RequestCommand;
import org.openeai.jms.consumer.commands.RequestCommandImpl;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.moa.ActionableEnterpriseObject;
import org.openeai.moa.EnterpriseObjectDeleteException;
import org.openeai.moa.EnterpriseObjectQueryException;
import org.openeai.moa.XmlEnterpriseObjectException;
import org.openeai.xml.XmlDocumentReader;
import org.openeai.xml.XmlDocumentReaderException;
import org.rhedcloud.moa.jmsobjects.tagging.v1_0.ResourceTaggingProfile;
import org.rhedcloud.moa.objects.resources.v1_0.ResourceTaggingProfileQuerySpecification;

public class Delete {

	public static void main(String[] args) throws EnterpriseConfigurationObjectException, EnterpriseObjectQueryException, XmlEnterpriseObjectException, EnterpriseFieldException, EnterpriseObjectDeleteException {
		System.out.println("Loading AppConfig");
		AppConfig appConfig = new AppConfig(args[0],args[1]);
		System.out.println("Done loading AppConfig");
		PointToPointProducer p2p = 
				(PointToPointProducer) appConfig.getObject("QueryAppP2PProducer");
		ResourceTaggingProfileQuerySpecification querySpec = 
				(ResourceTaggingProfileQuerySpecification) appConfig.getObject("ResourceTaggingProfileQuerySpecification.v1_0");
		ResourceTaggingProfile aeo = (ResourceTaggingProfile) appConfig.getObject("ResourceTaggingProfile.v1_0");
		aeo.setProfileName("waitsanddelays");
		aeo.setNamespace("cimp");
		aeo.setRevision("1");
		aeo.setProfileId("1");
		System.out.println(aeo.delete("true", p2p));
		System.exit(0);


	}

}
