# rhedcloud-resource-tagging-profile-service

##Copyright

This software was created at Emory University. Please see NOTICE.txt for Emory's copyright and disclaimer notifications. This software was extended and modified by the RHEDcloud Foundation and is copyright 2019 by the RHEDcloud Foundation.

##License

This software is licensed under the Apache 2.0 license included in this distribution as LICENSE-2.0.txt

## Resource Tagging Profile Service
The RTP service is used to persist the messages defined in the [org.rhedcloud.Tagging](https://bitbucket.org/rhedcloud/rhedcloud-moas/src/master/message/releases/org/rhedcloud/Tagging/) category of the [rhedcloud-moa](https://bitbucket.org/rhedcloud/rhedcloud-moas/src/master/).

This service is used by the [TagPolicyViolationDetector](https://bitbucket.org/rhedcloud/rhedcloud-srd-service/src/master/src/main/java/edu/emory/it/services/srd/detector/TagPolicyViolationDetector.java) of the [Security Risk Detector Service](https://bitbucket.org/rhedcloud/rhedcloud-srd-service/src/master/) to read tagging policies from persitent storage so as to enforce tagging policies on AWS objects. TODO: Link to TagPolicyViolationDetector doc if it exists.

The [RHEDcloud Console](https://bitbucket.org/rhedcloud/rhedcloud-vpcprovisioning-webapp/src/master/) is the primary application used to maintain the data in the RTP service.  

The service uses the [RdbmsRequestCommand](https://bitbucket.org/itarch/openeai-toolkit/src/master/src/main/java/com/openii/openeai/toolkit/rdbms/RdbmsRequestCommand.java) of the OpenEai Toolkit to implement CRUD functionality for this service. This command uses Hibernate and the hibernate mapping files to provide the persistence layer for the JMS messages managed by this service. The mapping files are stored in the [src/main/resources](src/main/resources).

The java code in this repo exists only for demonstration purposes.

## Pipelines ###
There are three separate build pipelines.

### Default Pipeline
The default pipeline runs automatically whenever there is a commit to the repo. 
This pipeline creates a jar file that contains the hibernate mapping files used by the RdbmsRequestCommand to persist and retrieve the messages. The jar file also contains the demo app classes. The jar file is saved to the downloads section on bitbucket and it and a zip of the entire repo are uploaded to the rhedcloud-bitbucket-downloads bucket in S3.

### Generating the hibernate mapping files
[ServiceGen](https://bitbucket.org/itarch/openeai-servicegen/src/master/) is used to generate the service artifacts including the hibernate mapping files. These files have already been generated from the MOA and are located in [src/main/resources](src/main/resources). If for some reason you need to regenertate these files, you can do so by running the `custom: gen-service` pipeline. The ServiceGen `target` folder is saved in the artifacts section of the pipeline build.

### Generating the WSDL
[ServiceGen](https://bitbucket.org/itarch/openeai-servicegen/src/master/) is used to generate the WSDL files for use with the [SOAP Facade](https://bitbucket.org/rhedcloud/rhedcloud-relay-servlet/src/master/). These files can be generated by running the `custom: gen-WSDL` pipeline. The WSDL is then saved to the downloads section for the repo. The name is `rhedcloud-moas-rtp-service-wsdl-master-###.zip` where `###` represents the build number.

## Updating the MOA
Replace the current moa jar in [lib](lib).

## DBConnection Information
The hibernate config file contains 6 placeholders that need to be replaced by the deploy-only pipeline. The hibernate config file is located at [config/hibernate-rtp.cfg.xml](config/hibernate-rtp.cfg.xml) 

The placeholder strings are
>CONNECTION\_URL  
>CONNECTION\_USERNAME  
>CONNECTION\_PASSWORD  
>CONNECTION\_HBM2DDL\_AUTO  
>CONNECTION\_DRIVER\_CLASS  
>CONNECTION\_DIALECT

## Demo apps
There are 6 apps in [src/main/java/org/rhedcloud/rtp/app](src/main/java/org/rhedcloud/rtp/app/)

Demo  | Description
------------- | :------------
Create  | Example of creating a profile
Create All  | Creates ResourceTaggingProfile objects in the service using the serialized objects in [config/ResourceTaggingProfile-SampleData.xml](config/ResourceTaggingProfile-SampleData.xml)  
Delete  | Example of deleting a profile
Delete All | Deletes all the profiles
Query  | Example of querying for profiles
Update  | Example of query and update

### Setting up for demo apps
The app config for the demo apps is specific to the deployment. For instance, the CIMP deployment app config is called `helperAppSonicAppConfig.xml` and can be downloaded from [confluence](https://serviceforge.atlassian.net/wiki/spaces/IAR/pages/597458945/Secrets).

## Notes
* To start the service locally: ant serviceHibernate
* To test it: ant testsuite
* You need to also start activemq locally to test it